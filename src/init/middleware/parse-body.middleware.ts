/**
 * Parses request body to json
 * @param req 
 * @param res 
 * @param next 
 */
export async function ParseBodyMiddleware(req, res, next) {
  req. body = req.body && typeof req.body === 'string' ? JSON.parse(req.body) : req.body;
  next();
}
