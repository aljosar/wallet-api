import { pgsqlService } from '../../services/postgresql-service';

/**
 * Returns all wallets
 */
export async function getWallets() {
  try {
    // success
    const wallets = await pgsqlService.db.any('SELECT * FROM wallets');
    return wallets;
  }
  catch (e) {
    // error
    throw new Error(e);
  }
}

/**
 * Creates new wallet with name and balance, returns id
 * @param name 
 * @param balance 
 */
export async function createNewWallet(name: string, balance: number) {
  try {
    // success
    const res = await pgsqlService.db.one('INSERT INTO wallets(name, balance) VALUES($<name>, $<balance>) RETURNING id', {
      name,
      balance
    });
    return res;
  }
  catch (e) {
    // error
    throw new Error(e);
  }
}

/**
 * Returns wallet based on id
 * @param id wallet id
 */
export async function getWalletById(id: number) {
  try {
    // success
    const wallet = await pgsqlService.db.one('SELECT * FROM wallets WHERE id = $<id>', { id });
    return wallet;
  }
  catch (e) {
    // error
    throw new Error(e);
  }
}

/**
 * Triggers transaction, returns wallet and transaction ids
 * @param wallet_id 
 * @param type 
 * @param amount 
 */
export async function transaction(wallet_id: number, type: 'deposit' | 'withdraw', amount: number) {
  try {
    const TransactionMode = pgsqlService.pgp.txMode.TransactionMode;
    const isolationLevel = pgsqlService.pgp.txMode.isolationLevel;

    // Create a reusable transaction mode (serializable):
    const mode = new TransactionMode({
      tiLevel: isolationLevel.serializable, // none, serializable, repeatableRead, readCommitted
      readOnly: false
    });

    const data = await pgsqlService.db.tx({ mode }, async t => {
      // `t` and `this` here are the same;
      // this.ctx = transaction config + state context;

      const walletId = await t.one(type === 'withdraw'
        ? 'UPDATE wallets SET balance = balance - $<amount> WHERE id = $<id> RETURNING id'
        : 'UPDATE wallets SET balance = balance + $<amount> WHERE id = $<id> RETURNING id', {
          id: wallet_id,
          amount
        }
      );
      const transactionId = await t.one('INSERT INTO transactions(wallet_id, type, amount, timestamp) VALUES($<wallet_id>, $<type>, $<amount>, $<timestamp>) RETURNING id', {
        wallet_id,
        type,
        amount,
        timestamp: new Date().toISOString()
      });

      return { walletId, transactionId };
    })
    // success
    return data
  }
  catch (e) {
    // error
    throw new Error(e);
  }
}

/**
 *  Returns transaction history
 */
export async function getHistory() {
  try {
    // success
    const transactions = await pgsqlService.db.any('SELECT * FROM transactions');
    return transactions;
  }
  catch (e) {
    // error
    throw new Error(e);
  }
}
/**
 *  Returns transaction history by wallet id
 * @param id wallet id
 */
export async function getHistoryById(id: number) {
  try {
    // success
    const transactions = await pgsqlService.db.any('SELECT * FROM transactions WHERE wallet_id = $<id>', { id });
    return transactions;
  }
  catch (e) {
    // error
    throw new Error(e);
  }
}