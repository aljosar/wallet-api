import * as Fastify        from 'fastify';
import { FastifyInstance } from 'fastify';
import { registerRoutes }  from '../modules/index';
import * as FastifyCors from 'fastify-cors';

const port = process.env.API_PORT || 3000;

export class Server {
  public fastify: FastifyInstance;

  constructor() {
    this.fastify = Fastify({
      logger: true,
    });
    
    // cors
    this.fastify.register(FastifyCors, {
      origin: 'http://localhost:4200'
    });

    this.fastify.addContentTypeParser('*', (req, done) => {
      done(null, req);
    });

    //index
    this.fastify.get('/', async (request, reply) => {
      return reply.send('Wallet app');
    });

    registerRoutes(this.fastify);
  }

  listen() {
    this.fastify.listen(Number(port), '0.0.0.0', (err, address) => {
      console.log(`Server started on ${address}`)
      if (err) {
        throw err;
      }
    });
  }
}

export const server = new Server();
export const logger = server.fastify.log;
