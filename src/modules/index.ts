import { FastifyInstance } from 'fastify';
import { WalletsRoutes } from './wallets-route';
import { AuthMiddleware } from '../init/middleware/auth.middleware';
import { ParseBodyMiddleware } from '../init/middleware/parse-body.middleware'

export function registerRoutes(fastify: FastifyInstance) {
  // parse body to json
  fastify.use(ParseBodyMiddleware);
  fastify.use(AuthMiddleware);

  fastify.register(WalletsRoutes, { prefix: '/wallets' });
}
