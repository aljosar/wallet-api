import * as pgPromise from 'pg-promise';

export class PostgreSQLService {
  // postgres://user:password@host:port/database
  url = `postgres://${process.env.DB_USER || 'postgres'}:${process.env.DB_PASSWORD || 'asdewq3456'}@${process.env.DB_HOST || 'localhost'}:${process.env.DB_PORT || '5432'}/${process.env.DB_NAME || 'test'}`;
  pgp;
  db;

  constructor() {
    this.pgp = pgPromise({/* Initialization Options */ });
    this.db = this.pgp(this.url);
  }
}

export const pgsqlService = new PostgreSQLService();