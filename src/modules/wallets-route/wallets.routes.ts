import { FastifyInstance } from 'fastify';
import { getWallets, createNewWallet, getWalletById, getHistoryById, transaction, getHistory }  from './wallets.service';

export const WalletsRoutes = (fastify: FastifyInstance, opts: any, next: any) => {
  fastify.get('/', async (request, response) => {
    const res = await getWallets();
    return response.status(200).send(res);
  });

  fastify.get('/history', async (request, response) => {
    const res = await getHistory();
    return response.status(200).send(res);
  });

  fastify.get('/:id', async (request, response) => {
    if (!request.params.id) {
      return response.status(400).send('Missing id');
    }
    const res = await getWalletById(request.params.id);
    return response.status(200).send(res);
  });

  fastify.get('/:id/history', async (request, response) => {
    if (!request.params.id) {
      return response.status(400).send('Missing id');
    }
    const res = await getHistoryById(request.params.id);
    return response.status(200).send(res);
  });

  fastify.post('/', async (request, response) => {
    if (!request.body.name || !request.body.balance) {
      return response.status(400).send('Missing parameters');
    }
    const res = await createNewWallet(request.body.name, request.body.balance);
    return response.status(201).send(res);
  });

  fastify.post('/:id/deposit', async (request, response) => {
    if (!request.params.id || !request.body.amount) {
      return response.status(400).send('Missing parameters');
    }
    const res = await transaction(request.params.id, 'deposit', request.body.amount);
    return response.status(202).send(res);
  });

  fastify.post('/:id/withdraw', async (request, response) => {
    if (!request.params.id || !request.body.amount) {
      return response.status(400).send('Missing parameters');
    }
    const res = await transaction(request.params.id, 'withdraw', request.body.amount);
    return response.status(202).send(res);
  });

  next();
};
