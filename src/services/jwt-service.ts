import * as jwt from 'jsonwebtoken';

export class JwtService {
    secret = process.env.JWT_SECRET || 'supersecret';

    async sign(data) {
        return await jwt.sign(data, this.secret);
    }

    async verify(token) {
        return await jwt.verify(token, this.secret);
    }
}