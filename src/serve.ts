import { server } from './init/server';
import * as WebSocket from 'ws';
import { createNewWallet, getWalletById, transaction, getHistory } from './modules/wallets-route';
import { IncomingMessage } from 'http';

//initialize the WebSocket server instance
const wss = new WebSocket.Server({
  server: server.fastify.server
});

wss.on('connection', (ws: any, request: IncomingMessage) => {
  ws.isAlive = true;
  ws.on('pong', () => {
    ws.isAlive = true;
  });

  //connection is up
  ws.on('message', async (message: string) => {
    const config = typeof message === 'string' ? JSON.parse(message) : message;

    let res;
    switch (config.action) {
      case 'new':
        res = !config.name || !config.balance ? 'Missing parameters' : await createNewWallet(config.name, config.balance);
        break;
      case 'status':
        res = !config.id ? 'Missing parameters' : await getWalletById(config.id);
        break;
      case 'deposit':
        res = !config.id || !config.amount ? 'Missing parameters' : await transaction(config.id, 'deposit', config.amount);
        break;
      case 'withdraw':
        res = !config.id || !config.amount ? 'Missing parameters' : await transaction(config.id, 'withdraw', config.amount);
        break;
      case 'history':
        res = await getHistory();
        break;
    }
    ws.send(JSON.stringify(res));
  });

  //send immediatly a feedback to the incoming connection
  ws.send('You are connected to wallet api!');
});

// handle broken connections
setInterval(() => {
  wss.clients.forEach((ws: any) => {

    if (!ws.isAlive) return ws.terminate();

    ws.isAlive = false;
    ws.ping(null, false, true);
  });
}, 10000);

// start fastify server
server.listen();