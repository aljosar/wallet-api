import { JwtService } from '../../services/jwt-service';

export async function AuthMiddleware(req, res, next) {
  // check request for jwt access key
  const jwt = req.headers['x-access-key'];

  if (!jwt) {
    res.statusCode = 403;
    res.end('Authorization token missing.');
  }
  // verify jwt end request if missing or invalid 
  const jwtService = new JwtService();

  try {
    // get user id from token
    const data: any = await jwtService.verify(jwt);
    if (!data) {
      res.statusCode = 403;
      res.end('Access denied - invalid token.');
    }

    // approve
    next();
  }
  catch (e) {
    res.statusCode = 403;
    res.end('Access denied.');
  }
}
