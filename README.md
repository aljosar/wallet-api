
# Wallet api by Aljoša Rakita (aljo.rakita@gmail.com)


This is a simple wallet application that stores balances for players and allows deposits and withdrawals. The application exposes a REST and a WebSocket API to fetch and update the player balances. Tech stack used is Node.js, Fastify, PostgreSQL(pg-promise) and WS library.


## Usage

  

REST:

(api has enabled authorization middleware that is why you need to include x-access-key header with jwt access token: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2Nlc3MiOnRydWUsImlhdCI6MTU4NzM3NjgxNH0.xnKjCMbwDLkqe9SV4l_ApFO0pi-57_9qJx-olM6paJc`)

  

* Retrieve list of wallets: __GET on /wallets__

* Retrieve list of all transactions: __GET on /wallets/history__

* Create new wallet: __POST on /wallets__ with body being JSON object with wallet name and balance. example body: `{"name": "Testak", "balance": "1337.13"}`

  

* Get wallet based on id: __GET on /wallets/:id__

* Get transaction history based on id: __GET on /wallets/:id/history__

* Deposit to wallet: __POST on /wallets/:id/deposit__ where :id is the id of the selected wallet with body being JSON object with ammount. example body: `{"amount": "1337.13"}`

* Withdraw from wallet: __POST on /wallets/:id/withdraw__ where :id is the id of the selected wallet with body being JSON object with ammount. example body: `{"amount": "1337.13"}`

  

WEBSOCKET:

To use api via websocket, client has to send JSON messages containing appropriate action and parameters needed for that action.

* __Get__ wallet based on id. Message: `{"action": "status", "id": "1"}`
* __Deposit__ to wallet. Message: `{"action": "deposit", "id": "1", "amount": "1337.13"}`
* __Withdraw__ from wallet. Message: `{"action": "withdraw", "id": "1", "amount": "1337.13"}`
* Retrieve list of __transaction history__. Message: `{"action": "history"}`
  

## Initialization and setup

  

To run application locally you first have to setup local postgresql server with database and table scripts bellow:

  

__Create database:__

```

CREATE DATABASE test

WITH

OWNER = postgres

ENCODING = 'UTF8'

LC_COLLATE = 'en_US.UTF-8'

LC_CTYPE = 'en_US.UTF-8'

TABLESPACE = pg_default

CONNECTION LIMIT = -1;

```

  

__Create transactions table:__

```

CREATE TABLE public.transactions

(

id integer NOT NULL DEFAULT nextval('transactions_id_seq'::regclass),

wallet_id integer NOT NULL,

type text COLLATE pg_catalog."default" NOT NULL,

amount double precision NOT NULL,

"timestamp" timestamp without time zone NOT NULL,

CONSTRAINT transactions_pkey PRIMARY KEY (id)

)

  

TABLESPACE pg_default;

  

ALTER TABLE public.transactions

OWNER to postgres;

```

  

__Create wallets table:__

  

```

CREATE TABLE public.wallets

(

id integer NOT NULL DEFAULT nextval('wallets_id_seq'::regclass),

name text COLLATE pg_catalog."default" NOT NULL,

balance double precision NOT NULL,

CONSTRAINT wallets_pkey PRIMARY KEY (id)

)

  

TABLESPACE pg_default;

  

ALTER TABLE public.wallets

OWNER to postgres;

```

  

__Enviroment variables:__

  

You can set custom variables for postgresql server connection:

  

* DB_HOST database host (default localhost)

* DB_PORT database port (default 5432)

* DB_NAME database name (default test)

* DB_USER psql username (default postgres)

* DB_PASSWORD psql password (default asdewq3456)



__Then install, build and run the app:__

```

npm install && npm run build:ts && npm run start

```

## Possible production improvements

* Propper type checking and validation
* Secure ws interaction (possibly also with jwt)
* Setup propper cors